#ifndef STEERINGSM_DEF
#define STEERINGSM_DEF

#include <stdint.h>
#include <stdbool.h>
#include "../../xf/xf.h"

#define STEERING_POWER_TM 100
#define STEERING_SPEED_TM 250

/*
 * these are the events of the
 * steering state machine
 * be sure to make the first event 
 * in the enumeration different from 0
 */
typedef enum STEvent 
{ 
    evSTInit = 20,
    evSTDef,     
    evSTSendTM,
    evSTisInit,
    evSTisHome,
    evSTCheckHome,
    evSTCheckCenter,
    evSTisCentered,
    evSTStop
} STEvent;

/*
 * these are the states of the
 * steering state machine 
 */
typedef enum STState
{
    ST_ST_START, 
    ST_ST_INIT,
    ST_ST_HOMING,
    ST_ST_CHECKHOMING,
    ST_ST_CENTER,
    ST_ST_CHECKCENTER,
    ST_ST_WAIT,
    ST_ST_SEND,
    ST_ST_STOPPED
} STState;

/*
 * the associated steering will be polled
 * each 50 ms. do not make this time 
 * shorter than TICKINTERVAL
 */
#define POLLTM 50


/*
 * this is the declaration of the SteeringSM class
 */
typedef struct SteeringSM
{
    STState state;
}SteeringSM;



void SteeringSM_init(SteeringSM* me);
void SteeringSM_startBehaviour(SteeringSM* me);
bool SteeringSM_processEvent(Event* ev);
#endif
