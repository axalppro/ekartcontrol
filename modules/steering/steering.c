#include "../../ekart/ekart.h"

void Steering_init(Steering* me)
{
    me->actual_pos = 0;
    me->center_pos = 0;
    me->target_pos = 0;
    me->homing = 0;         //Homing process never called
    me->status = 2;         //System not initialized
}

/**
 * @brief Initialize the Driver
 * 
 */
void Steering_initHW(Steering* me)
{
    //Setup steering and get center pos
    //Filling the id
    CanIdParser cip;
    cip.cid.dest = STEERINGID;
    cip.cid.src = CONTROLID;
    cip.cid.msgNbr = 0;
    me->aMsg.frame.id = cip.raw;

    //Filling the datas
    me->aMsg.frame.dlc = 4;
    me->aMsg.frame.rtr = 0;
    me->aMsg.frame.data0 = 1; //init
    me->aMsg.frame.data1 = 1; //homing
    me->aMsg.frame.data2 = 0;//set center;
    me->aMsg.frame.data3 = 100;

    CAN_transmit(&me->aMsg);
}


void Steering_initHOME(Steering* me)
{

}
void Steering_initCENTER(Steering* me)
{
    //Send setCenter + ask pos
    //Setup steering and get center pos
    //Filling the id
    CanIdParser cip;
    cip.cid.dest = STEERINGID;
    cip.cid.src = CONTROLID;
    cip.cid.msgNbr = 0;
    me->aMsg.frame.id = cip.raw;

    //Filling the datas
    me->aMsg.frame.dlc = 4;
    me->aMsg.frame.rtr = 0;
    me->aMsg.frame.data0 = 0; //init
    me->aMsg.frame.data1 = 0; //homing
    me->aMsg.frame.data2 = 1;//set center;
    me->aMsg.frame.data3 = 100;

    CAN_transmit(&me->aMsg);


    //Steering_getCenter(st());

}
void Steering_sendPos(Steering* me)
{
    //Filling the id
    CanIdParser cip;
    cip.cid.dest = STEERINGID;
    cip.cid.src = CONTROLID;
    cip.cid.msgNbr = 1;
    me->aMsg.frame.id = cip.raw;

    //Computing the target pos
    int32_t calc = Joystick_getPosX(js()) * me->center_pos;
    calc = calc / 100;
    uint32_t tpos = me->center_pos + calc;

    //Filling the datas
    me->aMsg.frame.dlc = 4;
    me->aMsg.frame.rtr = 0;
    me->aMsg.frame.data0 = (tpos & 0xFF000000)>>24; //HH
    me->aMsg.frame.data1 = (tpos & 0x00FF0000)>>16; //H
    me->aMsg.frame.data2 = (tpos & 0x0000FF00)>>8;  //L
    me->aMsg.frame.data3 = (tpos & 0x000000FF);     //LL

    CAN_transmit(&me->aMsg);
}

void Steering_setTargetPos(Steering* me, uint32_t t_pos)
{
    me->target_pos = t_pos;
}

//id getter
uint8_t Steering_getHoming(Steering* me)
{
    return me->homing;
}

//id getter
uint8_t Steering_getStatus(Steering* me)
{
    return me->status;
}

uint32_t Steering_getCenterPos(Steering* me)
{
    return me->center_pos;
}

//id getter
bool Steering_getMode(Steering* me)
{
    return me->mode;
}

void Steering_getCenter(Steering* me)
{
    //Setup steering and get center pos
    //Filling the id
    CanIdParser cip;
    cip.cid.dest = STEERINGID;
    cip.cid.src = SETUPID;
    cip.cid.msgNbr = 2;
    me->aMsg.frame.id = cip.raw;

    //Filling the datas
    me->aMsg.frame.dlc = 0;
    me->aMsg.frame.rtr = 1;

    CAN_transmit(&me->aMsg);
}