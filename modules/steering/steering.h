#ifndef Steering_ONCE
#define Steering_ONCE

#include <stdint.h>
#include <stdbool.h>




/*
 * this is the declaration of the Steering class
 */

typedef struct Steering
{
  uint32_t actual_pos;
  uint32_t center_pos;
  uint32_t target_pos;
  uint8_t homing;
  uint8_t status;
  bool mode;
  
  uCAN_MSG aMsg;
}Steering;


void Steering_init(Steering* me);
void Steering_initHW(Steering* me);
void Steering_initHOME(Steering* me);
void Steering_initCENTER(Steering* me);
void Steering_sendPos(Steering* me);
void Steering_setTargetPos(Steering* me, uint32_t t_pos);
uint8_t Steering_getHoming(Steering* me);
uint8_t Steering_getStatus(Steering* me);
uint32_t Steering_getCenterPos(Steering* me);
bool Steering_getMode(Steering* me);
void Steering_getCenter(Steering* me);

#endif
