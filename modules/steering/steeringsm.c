#include "steeringsm.h"
#include "../../ekart/ekart.h"

//extern Ekart theEkart;

/*
 * this is the init method of the SteeringSM class
 */
void SteeringSM_init(SteeringSM* me)
{
    me->state = ST_ST_START;
}

/*
 * this is the start method for the 
 * state machine of the SteeringSM class
 */
void SteeringSM_startBehaviour(SteeringSM* me)
{
    POST(me, &SteeringSM_processEvent, evSTInit,0,0);
}

uint32_t tmp_pos = 0;
/*
 * this is the state machine method of the SteeringSM class
 */
bool SteeringSM_processEvent(Event* ev)
{
    SteeringSM* me = (SteeringSM*)ev->target;
    bool processed = false;
    STState oldState = me->state;
      
    switch (me->state)
    {
        case ST_ST_START:
            if (Event_getId(ev) == evSTInit)
            {
                me->state = ST_ST_INIT;
            } 
        break;
        case ST_ST_INIT:
                if (Event_getId(ev) == evSTDef)
            {
                me->state = ST_ST_HOMING;
            }           
        break;

        case ST_ST_HOMING:
            if(Event_getId(ev) == evSTCheckHome)
            {
                me->state = ST_ST_CHECKHOMING;
            }
        break;

        case ST_ST_CHECKHOMING:
            if(Event_getId(ev) == evSTisHome)
            {
                me->state = ST_ST_CENTER;
                Steering_initCENTER(steer());
            }
            else if(Event_getId(ev) == evSTDef)
            {
                me->state = ST_ST_HOMING;
            }
        break;

        case ST_ST_CENTER:
            if(Event_getId(ev) == evSTCheckCenter)
            {
                me->state = ST_ST_CHECKCENTER;
            } 
        break;

        case ST_ST_CHECKCENTER:
            if(Event_getId(ev) == evSTisCentered)
            {
                POST(eksm(), EkartSM_processEvent, evEKSteeringReady,0,0);
                me->state = ST_ST_WAIT;
            }
            else if(Event_getId(ev) == evSTDef)
            {
                me->state = ST_ST_CENTER;
                Steering_getCenter(st());  
            }
        break;

        case ST_ST_WAIT:
            if(Event_getId(ev) == evSTSendTM)
            {
                me->state = ST_ST_SEND;
            }
            else if(Event_getId(ev) == evSTStop)
            {
                me->state = ST_ST_STOPPED;
            } 
        break;

        case ST_ST_SEND:
            if(Event_getId(ev) == evSTDef)
            {
                me->state = ST_ST_WAIT;
            }
            else if(Event_getId(ev) == evSTStop)
            {
                me->state = ST_ST_STOPPED;
            } 
        break;

        case ST_ST_STOPPED:
        break;

        
    }
    
    if (oldState != me->state)
    {
        processed = true;
        //Actions on entry
        switch (me->state)
        {
            case ST_ST_START:
            break;
            case ST_ST_INIT:
                Steering_initHW(steer());
                POST(steersm(), SteeringSM_processEvent, evSTDef,0,0);
            break;
            case ST_ST_HOMING:
            /*
                if(Steering_getHoming(steer()) == 2){
                    POST(steersm(), SteeringSM_processEvent, evSTisHome,0,0);
                }
                else
                {
                    POST(steersm(), SteeringSM_processEvent, evSTDef,POLLTM,0);
                }
                */
                //Steering_initHOME(steer());
                POST(steersm(), SteeringSM_processEvent, evSTCheckHome,POLLTM,0);
            break;

            case ST_ST_CHECKHOMING:
                if(Steering_getHoming(steer()) == 2){
                    POST(steersm(), SteeringSM_processEvent, evSTisHome,0,0);
                }
                else
                {
                    POST(steersm(), SteeringSM_processEvent, evSTDef,0,0);
                }
            break;
            case ST_ST_CENTER:   
                POST(steersm(), SteeringSM_processEvent, evSTCheckCenter,1000,0);        
            break;

            case ST_ST_CHECKCENTER:
                tmp_pos = Steering_getCenterPos(steer());                
                if(tmp_pos > 0)
                {
                    POST(steersm(), SteeringSM_processEvent, evSTisCentered,1000,0);
                }
                else
                {
                    Steering_getCenter(st());
                    POST(steersm(), SteeringSM_processEvent, evSTDef,0,0);
                }
            break;
            case ST_ST_WAIT:
                POST(me, &SteeringSM_processEvent, evSTSendTM,SENDTM,0);  
            break;

            case ST_ST_SEND:
                Steering_sendPos(steer());
                POST(me, &SteeringSM_processEvent, evSTDef,0,0);  
            break;

            case ST_ST_STOPPED: 
                //wait for state machine restart             
            break;
        }

        //On exit
        switch (oldState)
        {
            case ST_ST_START:
            break;
            case ST_ST_INIT:
            break;
            case ST_ST_HOMING:
            break;
            case ST_ST_CENTER: 
            break;
            case ST_ST_WAIT:  
            break;
            case ST_ST_SEND: 
            break;
            case ST_ST_STOPPED: 
                //wait for state machine restart             
            break;
        }
    }

    
    //Action to do while in state
    switch (me->state)
        {
            case ST_ST_START:
            break;
            case ST_ST_INIT:
            break;
            case ST_ST_HOMING:
            break;
            case ST_ST_CENTER:                
            break;
            case ST_ST_WAIT:
            break;
            case ST_ST_SEND:
            break;
            case ST_ST_STOPPED: 
                //wait for state machine restart             
            break;
        }
    return processed;
}



