#include "../../ekart/ekart.h"

void Drive_init(Drive* me)
{
    //me->power_factor = Store_read(st(), EE_POWER_FACTOR);
    //me->speed_factor = Store_read(st(), EE_SPEED_FACTOR);
    me->speed_rpm = 0;
    me->target_power = 0;
}

/**
 * @brief Initialize the Driver
 * 
 */
void Drive_initHW(Drive* me)
{
    //Send DRIVE_SETUP
    //Filling the id
    CanIdParser cip;
    cip.cid.dest = DRIVEID;
    cip.cid.src = CONTROLID;
    cip.cid.msgNbr = 0;
    me->aMsg.frame.id = cip.raw;

    //Filling the datas
    me->aMsg.frame.dlc = 4;
    me->aMsg.frame.data0 = 1;
    me->aMsg.frame.data1 = DRIVE_SPEED_SEND;    
    me->aMsg.frame.data2 = 0;//DRIVE_POWER_SEND;
    me->aMsg.frame.data3 = 100;

    CAN_transmit(&me->aMsg);
    
}


//Power Factor getter
uint32_t Drive_getPowerFactor(Drive* me)
{
    return me->power_factor;
}
//Speed Factor getter
uint32_t Drive_getSpeedFactor(Drive* me)
{
    return me->speed_factor;
}
//Speed RPM getter
int32_t Drive_getSpeedRPM(Drive* me)
{
    return me->speed_rpm;
}


//To send power value via CAN
void Drive_sendPower(Drive* me)
{
    //Filling the id
    CanIdParser cip;
    cip.cid.dest = DRIVEID;
    cip.cid.src = CONTROLID;
    cip.cid.msgNbr = 1;
    me->aMsg.frame.id = cip.raw;

    //Conversion
    int32_t fct = Store_read(st(), EE_POWER_FACTOR)<<24;
    fct += Store_read(st(), EE_POWER_FACTOR+1)<<16;
    fct += Store_read(st(), EE_POWER_FACTOR+2)<<8;
    fct += Store_read(st(), EE_POWER_FACTOR+3);
    fct = fct/1000;

    int16_t pwr = Joystick_getPosY(js()) * fct;
    
    //Filling the datas
    me->aMsg.frame.dlc = 2;
    me->aMsg.frame.data0 = (pwr & 0xFF00)>>8;
    me->aMsg.frame.data1 = (pwr & 0x00FF);

    CAN_transmit(&me->aMsg);
}

void Drive_sendSpeed(Drive* me)
{
    //Filling the id
    CanIdParser cip;
    cip.cid.dest = DISPLAYID;
    cip.cid.src = CONTROLID;
    cip.cid.msgNbr = 2;
    me->aMsg.frame.id = cip.raw;

    //Conversion
    int32_t fct = Store_read(st(), EE_SPEED_FACTOR)<<24;
    fct += Store_read(st(), EE_SPEED_FACTOR+1)<<16;
    fct += Store_read(st(), EE_SPEED_FACTOR+2)<<8;
    fct += Store_read(st(), EE_SPEED_FACTOR+3);
    fct = fct/1000;

    int16_t spd = me->speed_rpm/fct;
    
    //Filling the datas
    me->aMsg.frame.dlc = 2;
    me->aMsg.frame.data0 = (spd & 0xFF00)>>8;
    me->aMsg.frame.data1 = (spd & 0x00FF);

    CAN_transmit(&me->aMsg);
}

void Drive_stopMotor(Drive* me)
{
    //Filling the id
    CanIdParser cip;
    cip.cid.dest = DRIVEID;
    cip.cid.src = CONTROLID;
    cip.cid.msgNbr = 1;
    me->aMsg.frame.id = cip.raw;

    //Filling the datas
    me->aMsg.frame.dlc = 2;
    me->aMsg.frame.data0 = 0;
    me->aMsg.frame.data1 = 0;

    CAN_transmit(&me->aMsg);
}