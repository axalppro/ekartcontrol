#include "drivesm.h"
#include "../../ekart/ekart.h"

//extern Ekart theEkart;

/*
 * this is the init method of the DriveSM class
 */
void DriveSM_init(DriveSM* me)
{
    me->state = ST_DR_START;

    me->actualState = ST_DR_START;
}

/*
 * this is the start method for the 
 * state machine of the DriveSM class
 */
void DriveSM_startBehaviour(DriveSM* me)
{
    POST(me, &DriveSM_processEvent, evDRInit,0,0);
}

/*
 * this is the state machine method of the DriveSM class
 */
bool DriveSM_processEvent(Event* ev)
{
    DriveSM* me = (DriveSM*)ev->target;
    bool processed = false;
    DRState oldState = me->state;
      
    switch (me->state)
    {
        case ST_DR_START:
            if (Event_getId(ev) == evDRInit)
            {
                me->state = ST_DR_INIT;
            } 
        break;
        case ST_DR_INIT:
                if (Event_getId(ev) == evDRDef)
            {
                me->state = ST_DR_WAIT;
                POST(eksm(), EkartSM_processEvent, evEKDef,0,0);
            }           
        break;

        case ST_DR_WAIT:
            if(Event_getId(ev) == evDRBrakeOn)
            {
                me->state = ST_DR_BRAKE;
            }
            else if (Event_getId(ev) == evDRSendTM)
            {
                me->state = ST_DR_SEND;
            }  
        break;

        case ST_DR_SEND:
            if(Event_getId(ev) == evDRBrakeOn)
            {
                me->state = ST_DR_BRAKE;
            }
            else if (Event_getId(ev) == evDRStp)
            {
                me->state = ST_DR_STOP;
            }
            else if (Event_getId(ev) == evDRFwd)
            {
                me->state = ST_DR_FORWARD;
            }    
            else if (Event_getId(ev) == evDRBwd)
            {
                me->state = ST_DR_BACKWARD;
            }   
        break;

        case ST_DR_FORWARD:
            if(Event_getId(ev) == evDRBrakeOn)
            {
                me->state = ST_DR_BRAKE;
            }
            else if (Event_getId(ev) == evDRDef)
            {
                me->state = ST_DR_WAIT;
            }   
        break;

        case ST_DR_STOP:
            if(Event_getId(ev) == evDRBrakeOn)
            {
                me->state = ST_DR_BRAKE;
            }
            else if (Event_getId(ev) == evDRDef)
            {
                me->state = ST_DR_WAIT;
            }   
        break;

        case ST_DR_BACKWARD:
            if(Event_getId(ev) == evDRBrakeOn)
            {
                me->state = ST_DR_BRAKE;
            }
            else if (Event_getId(ev) == evDRDef)
            {
                me->state = ST_DR_WAIT;
            }   
        break;

        case ST_DR_BRAKE:
            if (Event_getId(ev) == evDRBrakeOff)
            {
                me->state = ST_DR_INIT;
            }           
        break;
    }
    if (oldState != me->state)
    {
        processed = true;
        switch (me->state)
        {
            case ST_DR_START:
            break;
            case ST_DR_INIT:
                Drive_initHW(dr());
                POST(me, &DriveSM_processEvent, evDRDef,0,0);
            break;
            case ST_DR_WAIT:
                POST(me, &DriveSM_processEvent, evDRSendTM,DRIVE_POWER_TM,0);
            break;
            case ST_DR_SEND:
                if(Drive_getSpeedRPM(dr()) > 2)
                    POST(me, &DriveSM_processEvent, evDRFwd,0,0);  
                else if(Drive_getSpeedRPM(dr()) < -2)
                    POST(me, &DriveSM_processEvent, evDRBwd,0,0);  
                else
                    POST(me, &DriveSM_processEvent, evDRStp,0,0);  
            break;
            case ST_DR_STOP:
                Drive_sendPower(dr());
                POST(me, &DriveSM_processEvent, evDRDef,0,0);  
            break;

            case ST_DR_FORWARD:
                if(Joystick_getPosY(js()) <= 0){
                    Drive_stopMotor(dr());
                }                    
                else{
                    Drive_sendPower(dr());
                }
                    
                POST(me, &DriveSM_processEvent, evDRDef,0,0);  
            break;

            case ST_DR_BACKWARD:
                if(Joystick_getPosY(js()) >= 0){
                    Drive_stopMotor(dr());
                }                    
                else{
                    Drive_sendPower(dr());
                }                    

                POST(me, &DriveSM_processEvent, evDRDef,0,0);  
            break;
            
            case ST_DR_BRAKE:
                Drive_stopMotor(dr());
            break;
        }
    }
    return processed;
}



