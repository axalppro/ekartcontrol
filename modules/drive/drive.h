#ifndef Drive_ONCE
#define Drive_ONCE

#include <stdint.h>
#include <stdbool.h>

#define DRIVE_POWER_SEND 15
#define DRIVE_SPEED_SEND 25



/*
 * this is the declaration of the Drive class
 */

typedef struct Drive
{
  uint16_t target_power;
  int32_t speed_rpm;
  uint32_t speed_factor;
  uint32_t power_factor;
  uCAN_MSG aMsg;

}Drive;

void Drive_init(Drive* me);
void Drive_initHW(Drive* me);
void Drive_sendPower(Drive* me);
void Drive_sendSpeed(Drive* me);
void Drive_stopMotor(Drive* me);
uint32_t Drive_getPowerFactor(Drive* me);
uint32_t Drive_getSpeedFactor(Drive* me);
int32_t Drive_getSpeedRPM(Drive* me);

#endif
