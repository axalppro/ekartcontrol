#ifndef DRIVESM_DEF
#define DRIVESM_DEF

#include <stdint.h>
#include <stdbool.h>
#include "../../xf/xf.h"

#define DRIVE_POWER_TM 100
#define DRIVE_SPEED_TM 250

/*
 * these are the events of the
 * drive state machine
 * be sure to make the first event 
 * in the enumeration different from 0
 */
typedef enum DREvent 
{ 
    evDRInit = 30,
    evDRDef,     
    evDRSendTM,
    evDRFwd,
    evDRBwd,
    evDRStp,
    evDRBrakeOn,
    evDRBrakeOff
} DREvent;

/*
 * these are the states of the
 * drive state machine 
 */
typedef enum DRState
{
    ST_DR_START, 
    ST_DR_INIT,
    ST_DR_WAIT,
    ST_DR_SEND,
    ST_DR_STOP,
    ST_DR_FORWARD,
    ST_DR_BACKWARD,
    ST_DR_BRAKE
} DRState;

/*
 * the associated drive will be polled
 * each 50 ms. do not make this time 
 * shorter than TICKINTERVAL
 */
#define SENDTM 250


/*
 * this is the declaration of the DriveSM class
 */
typedef struct DriveSM
{
    DRState state;
    DRState actualState;

}DriveSM;



void DriveSM_init(DriveSM* me);
void DriveSM_startBehaviour(DriveSM* me);
bool DriveSM_processEvent(Event* ev);
#endif
