
#include "display.h"

void Display_init(Display* me)
{
    me->speed_kmh = 0;
}

/**
 * @brief Initialize the Driver
 * 
 */
void Display_initHW(Display* me)
{
}


//Speed getter
uint32_t Display_getSpeedKMH(Display* me)
{
    return me->speed_kmh;
}

//Speed setter
void Display_setSpeedKMH(Display* me, uint32_t spd)
{
    me->speed_kmh = spd;
}