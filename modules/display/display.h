#ifndef Display_ONCE
#define Display_ONCE

#include <stdint.h>
#include <stdbool.h>

/*
 * this is the declaration of the Display class
 */

typedef struct Display
{
  uint32_t speed_kmh;
}Display;


void Display_init(Display* me);
void Display_initHW(Display* me);
void Display_setSpeedKMH(Display* me, uint32_t spd);
uint32_t Display_getSpeedKMH(Display* me);

#endif
