#ifndef Joystick_ONCE
#define Joystick_ONCE

#include <stdint.h>
#include <stdbool.h>

/*
 * this is the declaration of the Joystick class
 */

typedef struct Joystick
{
  int8_t posX;
  int8_t posY;
  bool button;  
}Joystick;


void Joystick_init(Joystick* me);
void Joystick_initHW(Joystick* me);
int8_t Joystick_getPosX(Joystick* me);
int8_t Joystick_getPosY(Joystick* me);
bool Joystick_getButton(Joystick* me);
void Joystick_setPosX(Joystick* me, int8_t val);
void Joystick_setPosY(Joystick* me, int8_t val);
void Joystick_setButton(Joystick* me, bool val);

#endif
