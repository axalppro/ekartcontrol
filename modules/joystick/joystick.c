
#include "joystick.h"
#include "../../mcc_generated_files/pin_manager.h"

void Joystick_init(Joystick* me)
{
    me->posX = 0;
    me->posY = 0;
    me->button = 0;
}

/**
 * @brief Initialize the Driver
 * 
 */
void Joystick_initHW(Joystick* me)
{
}


//PosX getter
int8_t Joystick_getPosX(Joystick* me)
{
    return me->posX;
}
//PosY getter
int8_t Joystick_getPosY(Joystick* me)
{
    return me->posY;
}
//Button getter
bool Joystick_getButton(Joystick* me)
{
    return me->button;
}

void Joystick_setPosX(Joystick* me, int8_t val)
{
    me->posX = val;
}

void Joystick_setPosY(Joystick* me, int8_t val)
{
    me->posY = val;
}

void Joystick_setButton(Joystick* me, bool val)
{
    me->button = val;
}