#ifndef Supply_ONCE
#define Supply_ONCE

#include <stdint.h>
#include <stdbool.h>

/*
 * this is the declaration of the Supply class
 */

struct Supply_
{
};

typedef struct Supply_ Supply;

void Supply_init(Supply* me);
void Supply_initHW(Supply* me);

#endif
