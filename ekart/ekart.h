#ifndef Ekart_ONCE
#define Ekart_ONCE

#include <stdint.h>
#include <stdbool.h>
#include "../middleware/comcontrol.h"
#include "../middleware/store.h"
#include "../modules/joystick/joystick.h"
#include "../modules/display/display.h"
#include "../modules/drive/drive.h"
#include "../modules/drive/drivesm.h"
#include "../modules/steering/steering.h"
#include "../modules/steering/steeringsm.h"
#include "../modules/supply/supply.h"
#include "ekartsm.h"

/*
 * this is the declaration of the Button class
 */

typedef struct Ekart
{
    
        Joystick theJoystick;
        Display theDisplay;
        Drive theDrive;
        DriveSM theDriveSM;
        Steering theSteering;
        SteeringSM theSteeringSM;
        Supply theSupply;
        CommControl theCommControl;
        Store theStore;
        EkartSM theEkartSM;

}Ekart;


void Ekart_init();
void Ekart_build();
void Ekart_initHW();

//these are global getters for our objects
Joystick* js();
Display* dp();
Drive* dr();
DriveSM* drsm();
Steering* steer();
SteeringSM* steersm();
Supply* sup();
CommControl* comc();
Store* st();
EkartSM* eksm();

#endif
