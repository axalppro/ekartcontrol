#ifndef EKARTSM_DEF
#define EKARTSM_DEF

#include <stdint.h>
#include <stdbool.h>
#include "../xf/xf.h"

#define EKART_POWER_TM 100
#define EKART_SPEED_TM 250

/*
 * these are the events of the
 * steering state machine
 * be sure to make the first event 
 * in the enumeration different from 0
 */
typedef enum EKEvent 
{ 
    evEKInit = 40,
    evEKDcDcReady,
    evEKDef,     
    evEKSteeringReady,
    evEKJoyAlive,
    evEKJoyNotAlive,
    evEKSteerAlive,
    evEKSteerNotAlive,
    evEKSupplyAlive,
    evEKSupplyNotAlive,
    evEKDriveAlive,
    evEKDriveNotAlive
} EKEvent;

/*
 * these are the states of the
 * steering state machine 
 */
typedef enum EKState
{
    ST_EK_START,
    ST_EK_INIT, 
    ST_EK_OK_SUPPLY,
    ST_EK_OK_STEER,
    ST_EK_RUNNING,
    ST_EK_HS_DRIVE,
    ST_EK_HS_SUPPLY,
    ST_EK_HS_STEERING,
    ST_EK_STEERINIT,
    ST_EK_HS_JOYSTICK
} EKState;

/*
 * the associated steering will be polled
 * each 50 ms. do not make this time 
 * shorter than TICKINTERVAL
 */
#define POLLTM 50


/*
 * this is the declaration of the EkartSM class
 */
typedef struct EkartSM
{
    EKState state;
}EkartSM;



void EkartSM_init(EkartSM* me);
void EkartSM_startBehaviour(EkartSM* me);
bool EkartSM_processEvent(Event* ev);
#endif
