#include "ekartsm.h"
#include "ekart.h"

//extern Ekart theEkart;

/*
 * this is the init method of the EkartSM class
 */
void EkartSM_init(EkartSM* me)
{
    me->state = ST_EK_START;
}

/*
 * this is the start method for the 
 * state machine of the EkartSM class
 */
void EkartSM_startBehaviour(EkartSM* me)
{
    POST(me, &EkartSM_processEvent, evEKInit,0,0);
}

uint32_t tmp_pos = 0;
/*
 * this is the state machine method of the EkartSM class
 */
bool EkartSM_processEvent(Event* ev)
{
    EkartSM* me = (EkartSM*)ev->target;
    bool processed = false;
    EKState oldState = me->state;
      
    switch (me->state)
    {
        case ST_EK_START:
            if (Event_getId(ev) == evEKInit)
            {
                me->state = ST_EK_INIT;
            } 
        break;
        case ST_EK_INIT:
            if (Event_getId(ev) == evEKDcDcReady)
            {
                me->state = ST_EK_OK_SUPPLY;
            } 
        break;
        case ST_EK_OK_SUPPLY:
                if (Event_getId(ev) == evEKSteeringReady)
            {
                me->state = ST_EK_OK_STEER;
            }           
        break;

        case ST_EK_OK_STEER:
            if(Event_getId(ev) == evEKDef)
            {
                me->state = ST_EK_RUNNING;
            }
        break;

        case ST_EK_RUNNING:
            if(Event_getId(ev) == evEKSteerNotAlive)
            {
                me->state = ST_EK_HS_STEERING;
            }
            else if(Event_getId(ev) == evEKJoyNotAlive)
            {
                me->state = ST_EK_HS_JOYSTICK;
            }
            else if(Event_getId(ev) == evEKDriveNotAlive)
            {
                me->state = ST_EK_HS_DRIVE;
            }
            else if(Event_getId(ev) == evEKSupplyNotAlive)
            {
                me->state = ST_EK_HS_SUPPLY;
            }
        break;

        case ST_EK_HS_DRIVE:
            if(Event_getId(ev) == evEKDriveAlive)
            {
                me->state = ST_EK_OK_STEER;
            } 
        break;

        case ST_EK_HS_SUPPLY:
            if(Event_getId(ev) == evEKDcDcReady)
            {
                me->state = ST_EK_OK_SUPPLY;
            }
        break;

        case ST_EK_HS_STEERING:
            if(Event_getId(ev) == evEKSteerAlive)
            {
                me->state = ST_EK_STEERINIT;
            } 
        break;

        case ST_EK_STEERINIT:
            if(Event_getId(ev) == evEKSteeringReady)
            {
                me->state = ST_EK_RUNNING;
            } 
        break;

        case ST_EK_HS_JOYSTICK:
            if(Event_getId(ev) == evEKJoyAlive)
            {
                me->state = ST_EK_RUNNING;
            } 
        break;

        
    }
    
    if (oldState != me->state)
    {
        processed = true;
        //Actions on entry
        switch (me->state)
        {
            case ST_EK_START:
            break;
            case ST_EK_INIT:
                //Only for debugging while DCDC board not ready
                POST(me, EkartSM_processEvent, evEKDcDcReady,0,0);
            break;
            case ST_EK_OK_SUPPLY:
                SteeringSM_startBehaviour(steersm());
            break;
            case ST_EK_OK_STEER:
                DriveSM_startBehaviour(drsm());
            break;
            case ST_EK_RUNNING:                
            break;
            case ST_EK_HS_DRIVE:
                POST(drsm(), DriveSM_processEvent, evDRBrakeOn,0,0);
            break;
            case ST_EK_HS_SUPPLY:
                POST(drsm(), DriveSM_processEvent, evDRBrakeOn,0,0);
                POST(steersm(), SteeringSM_processEvent, evSTStop,0,0);
            break;
            case ST_EK_HS_STEERING:
                POST(drsm(), DriveSM_processEvent, evDRBrakeOn,0,0);
            break;
            case ST_EK_STEERINIT:
                SteeringSM_init(steersm());
                SteeringSM_startBehaviour(steersm());

            break;
            case ST_EK_HS_JOYSTICK:
                POST(drsm(), DriveSM_processEvent, evDRBrakeOn,0,0);
                POST(steersm(), SteeringSM_processEvent, evSTStop,0,0);
            break;
        }

        //On exit
        switch (oldState)
        {
            case ST_EK_INIT:
            break;
            case ST_EK_OK_SUPPLY:
            break;
            case ST_EK_OK_STEER:
            break;
            case ST_EK_RUNNING:                
            break;
            case ST_EK_HS_DRIVE:
                POST(drsm(), DriveSM_processEvent, evDRBrakeOff,0,0);
            break;
            case ST_EK_HS_SUPPLY:
                SteeringSM_init(steersm());
                DriveSM_init(drsm());
            break;
            case ST_EK_HS_STEERING:
            break;
            case ST_EK_STEERINIT:
                POST(drsm(), DriveSM_processEvent, evDRBrakeOff,0,0);
            break;
            case ST_EK_HS_JOYSTICK:
                POST(drsm(), DriveSM_processEvent, evDRBrakeOff,0,0);
            break;
        }
    }
    POST(me, &EkartSM_processEvent, evSTDef,0,0);  
    
    //Action to do each time
    switch (me->state)
        {
            case ST_EK_INIT:
            break;
            case ST_EK_OK_SUPPLY:
            break;
            case ST_EK_OK_STEER:
            break;
            case ST_EK_RUNNING:                
            break;
            case ST_EK_HS_DRIVE:
            break;
            case ST_EK_HS_SUPPLY:
            break;
            case ST_EK_HS_STEERING:
            break;
            case ST_EK_STEERINIT:
            break;
            case ST_EK_HS_JOYSTICK:
            break;
        }
    return processed;
}



