
#include "ekart.h"

static Ekart theEkart;

//Private methods
void Ekart_toggle_led();
 
void Ekart_init()
{
    Joystick_init(js());
    Display_init(dp());
    Drive_init(dr());
    DriveSM_init(drsm());
    Steering_init(steer());
    SteeringSM_init(steersm());
    Supply_init(sup());
    CommControl_init(comc());
    Store_init(st());    
    EkartSM_init(eksm());
}

void Ekart_build()
{
    //Start the comm state machine
    CommControl_startBehaviour(comc());

    //For debug
    POST(&theEkart, &Ekart_toggle_led, 11, 1000,0);
}

/**
 * @brief Initialize the Driver
 * 
 */
void Ekart_initHW()
{
    //Start the drive and steering state machines
    //DriveSM_startBehaviour(drsm());
    //SteeringSM_startBehaviour(steersm());
    EkartSM_startBehaviour(eksm());
}

//Used for debugging
void Ekart_toggle_led(){
    IO_RA0_Toggle();
    POST(&theEkart, &Ekart_toggle_led, 11, 1000,0);
}


//all the getters

Joystick* js(){
    return &theEkart.theJoystick;
}
Display* dp(){
    return &theEkart.theDisplay;
}
Drive* dr(){
    return &theEkart.theDrive;
}
DriveSM* drsm(){
    return &theEkart.theDriveSM;
}
Steering* steer(){
    return &theEkart.theSteering;
}
SteeringSM* steersm(){
    return &theEkart.theSteeringSM;
}
Supply* sup(){
    return &theEkart.theSupply;
}
CommControl* comc(){
    return &theEkart.theCommControl;
}
Store* st(){
    return &theEkart.theStore;
}

EkartSM* eksm(){
    return &theEkart.theEkartSM;
}





