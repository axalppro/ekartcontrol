/* this is the Factory class */


#ifndef FACTORY_ONCE
#define FACTORY_ONCE

#include <stdint.h>
#include <stdbool.h>

#include "../middleware/comcontrol.h"
#include "../middleware/store.h"
#include "../ekart/ekart.h"


#define BID 1
#define LID 1

void bObs(void*, uint8_t,bool);

struct Factory_
{
    CommControl comc_;
    Store st_;
    //Global ekart
    Ekart theEkart;
};

typedef struct Factory_ Factory;

void Factory_init();
void Factory_build();
void Factory_start();

//these are global getters for our objects
CommControl* comc();
Store* st();
Ekart* ek();

#endif