#include "factory.h"
#include "../mcc_generated_files/pin_manager.h"

//the factory object containing all objects of our system
static Factory theFactory;
//Global ekart
Ekart theEkart;

//Private methods
void Factory_toggle_relays();
void Factory_toggle_led();

//all the getters
CommControl* comc(){
    return &theFactory.comc_;
}

Store* st(){
    return &theFactory.st_;
}

Ekart* ek(){
    return &theFactory.theEkart;
}


//initialize all objects
void Factory_init()
{
    Store_init(st());
    CommControl_init(comc());
    Ekart_init(ek());
    
}



//connect objects if required
void Factory_build()
{
  //Start the comm state machine
    CommControl_startBehaviour(comc());
}

//start all state machines
void Factory_start()
{
    Ekart_initHW(&theEkart);

    //For debug
    POST(&theFactory, &Factory_toggle_led, 11, 1000,0);
    POST(&theFactory, &Factory_toggle_relays, 12, 10,0);
}

void Factory_toggle_led(){
    IO_RA0_Toggle();
    POST(&theFactory, &Factory_toggle_led, 11, 1000,0);
}

uint8_t index = 0;

void Factory_toggle_relays(){
  
  switch(index)
  {
    case 0: IO_RA2_SetHigh();
    break;
    case 1: IO_RA3_SetHigh();
    break;
    case 2: IO_RA4_SetHigh();
    break;
    case 3: IO_RA5_SetHigh();
    break;
    case 4: IO_RA6_SetHigh();
    break;
    case 5: IO_RA7_SetHigh();
    break;
    
    case 6: IO_RA2_SetLow();
    break;
    case 7: IO_RA3_SetLow();
    break;
    case 8: IO_RA4_SetLow();
    break;
    case 9: IO_RA5_SetLow();
    break;
    case 10: IO_RA6_SetLow();
    break;
    case 11: IO_RA7_SetLow();
    break;
    default:
    break;
  }
  if(index < 10)
  {
    if(index==5)
      POST(&theFactory, &Factory_toggle_relays, 12, 500,0);
    else
      POST(&theFactory, &Factory_toggle_relays, 12, 100,0);
  }
  index++;
  
}


