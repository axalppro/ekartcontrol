#ifndef Store_ONCE
#define Store_ONCE

#include <stdint.h>
#include <stdbool.h>
#include "../mcc_generated_files/mcc.h"

#define MAXEEVALUE 21

#define   EE_INIT 0                /*1byte*/
#define   EE_INIT2 22                /*1byte*/
#define   EE_STEERING_MODE 1       /*1byte*/
#define   EE_ALIVE_TM 2           /*1byte*/
#define   EE_SPEED_FACTOR 3       /*4byte*/
#define   EE_POWER_FACTOR 7        /*4byte*/
#define   EE_STEERING_FACTOR 11    /*4byte*/
#define   EE_MAX_SPEED_FWD 15      /*1byte*/
#define   EE_MAX_SPEED_BWD 16      /*1byte*/
#define   EE_CENTER_POS 17          /*4byte*/

///*
//typedef enum EEITEMID_ {  EE_INIT=0,                /*1byte*/
//                          EE_STEERING_MODE=1,       /*1byte*/
//                          EE_ALIVE_TM=2,            /*1byte*/
//                          EE_SPEED_FACTOR=3,        /*4byte*/
//                          EE_POWER_FACTOR=7,        /*4byte*/
//                          EE_STEERING_FACTOR=11,    /*4byte*/
//                          EE_MAX_SPEED_FWD=15,      /*1byte*/
//                          EE_MAX_SPEED_BWD=16,      /*1byte*/
//                          EE_CENTER_POS=17          /*4byte*/
//
//} EEITEMID;
//*/
        
struct Store_
{
    uint8_t toto;
    uint8_t eeValues[MAXEEVALUE];
};



typedef struct Store_ Store;

void Store_init(Store* me);
uint8_t Store_read(Store* me, uint16_t item);
void Store_write(Store* me, uint16_t item, uint8_t value);

#endif
