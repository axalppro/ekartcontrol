#include "comcontrol.h"
#include "../ekart/ekart.h"

//private methods

void readCanMessage(CommControl* me, uCAN_MSG* message);
void sendKeepAliveMessage(CommControl* me);
void storeSetupData(CommControl* me, uint8_t msgNbr, uCAN_MSG* message);
void storeJoystickData(CommControl* me,  uint8_t msgNbr,uCAN_MSG* message);
void storeDisplayData(CommControl* me, uint8_t msgNbr, uCAN_MSG* message);
void storeDriveData(CommControl* me, uint8_t msgNbr, uCAN_MSG* message);
void storeSteeringData(CommControl* me, uint8_t msgNbr, uCAN_MSG* message);
void storeSupplyData(CommControl* me, uint8_t msgNbr, uCAN_MSG* message);


void CommControl_init(CommControl* me)
{
    me->rxState = ST_RXINIT;
    me->txState = ST_TXINIT;
    
    me->txMsg.frame.idType = dSTANDARD_CAN_MSG_ID_2_0B;
    me->txMsg.frame.rtr = 0;
}

bool CommControl_processEvent(Event* ev)
{
    CommControl* me = (CommControl*) ev->target;
    bool processed = false;
    CCRXState oldRxState = me->rxState;
    CCTXState oldTxState = me->txState;


    //this is the transmitter state machine
    //it is priorized over the receiver sm

    switch (me->txState) {
        case ST_TXINIT:
            if (Event_getId(ev) == evTXInit)
            {
                me->txState = ST_TXWAIT;
            }
            break;
        case ST_TXWAIT:
            if (Event_getId(ev) == evTXSendAliveTm)
            {
                me->txState = ST_TXSENDALIVEMSG;
            }
            break;
        case ST_TXSENDALIVEMSG:
            if (Event_getId(ev) == evTXDefault)
            {
                me->txState = ST_TXWAIT;
            }
            break;
        default:
            break;
    }
    if (oldTxState != me->txState)
    {
        processed = true;
        switch (me->txState)
        {
            case ST_TXINIT:
                break;
            case ST_TXWAIT:
                POST(me, &CommControl_processEvent, evTXSendAliveTm,Store_read(st(), EE_ALIVE_TM)*TIMEFACTOR,0);                
                break;
            case ST_TXSENDALIVEMSG:
                POST(me, &CommControl_processEvent, evTXDefault,0,0);
                sendKeepAliveMessage(me);
                break;
            default:
                break;
        }
    }

    if (processed == false)
    {
        //this is the receiver state machine. 
        //it reads eventually incoming
        //can messages each 20 milliseconds and treats them
        switch (me->rxState)
        {
            case ST_RXINIT:
                if (Event_getId(ev) == evRXInit)
                {
                    me->rxState = ST_RXWAIT;
                }
                break;
            case ST_RXWAIT:
                if (Event_getId(ev) == evRXReadTm)
                {
                    me->rxState = ST_RXRCVMSG;
                }
                break;
            case ST_RXRCVMSG:
                if (Event_getId(ev) == evRXDefault)
                {
                    me->rxState = ST_RXWAIT;
                }
                break;
            default:
                break;
        }
        if (oldRxState != me->rxState)
        {
            processed = true;
            switch (me->rxState)
            {
                case ST_RXINIT:
                    break;
                case ST_RXWAIT:
                    POST(me, &CommControl_processEvent, evRXReadTm,RECEIVETM,0);
                    break;
                case ST_RXRCVMSG:
                    POST(me, &CommControl_processEvent, evRXDefault,0,0);
                    readCanMessage(me, &(me->rxMsg));
                    break;
                default:
                    break;
            }
        }
    }
    return processed;
}

void CommControl_startBehaviour(CommControl* me)
{
    POST(me, &CommControl_processEvent, evRXInit,0,0);
    POST(me, &CommControl_processEvent, evTXInit,0,0);
}

void readCanMessage(CommControl* me, uCAN_MSG* message)
{
    if (CAN_receive(message) != 0)
    {
        CanIdParser cip;
        cip.raw = message->frame.id;
        uint8_t source = cip.cid.src;
        uint8_t destination = cip.cid.dest;
        uint8_t messageNumber = cip.cid.msgNbr;
        
        switch (source) //we are only interested into the last 4 bits
                                         //that is why we and 
        {
            case SETUPID:
                storeSetupData(me, messageNumber, message);
            break;   

            case JOYSTICKID:
                storeJoystickData(me, messageNumber, message);
            break;

            case DISPLAYID:
                storeDisplayData(me, messageNumber, message);

            break;

            case DRIVEID:
                storeDriveData(me, messageNumber, message);

            break;

            case STEERINGID:
                storeSteeringData(me, messageNumber, message);

            break;

            case SUPPLYID:
                storeSupplyData(me, messageNumber, message);

            break;
            
            default: 
                // do nothing
               break;
        }
    }
}

void CommControl_sendCanMessage(CommControl* me, uCAN_MSG* message)
{
    CAN_transmit(message);
}

void sendKeepAliveMessage(CommControl* me)
{   
    /*
     * control keep alive speed with oscilloscope
    static bool on = false;
    if (on==true)
    {
        on = false;
        IO_RB0_SetHigh();
    }
    else
    {
        on = true;
        IO_RB0_SetLow();
    }
    */
    
    //send only if alive time > 0
    if (Store_read(st(),EE_ALIVE_TM) > 0)
    {    
        me->txMsg.frame.dlc = 0;
        me->txMsg.frame.id  = KEEPALIVEID;
        CommControl_sendCanMessage(me,&(me->txMsg));
    }
}

void storeSetupData(CommControl* me, uint8_t msgNbr, uCAN_MSG* message)
{
    uint32_t tmp = 0;
    
    //IO_RA1_Toggle();
    //Managing messages from the debug
    switch (msgNbr)
    {
    case 0: //CONTROL SETUP
        Store_write(st(),EE_STEERING_MODE,message->frame.data0);  //the steering mode
        Store_write(st(),EE_ALIVE_TM,message->frame.data3); //the alive time  
        break;

    case 1: //SPEED FACTOR
        Store_write(st(),EE_SPEED_FACTOR,message->frame.data0);  //the steering mode
        Store_write(st(),EE_SPEED_FACTOR+1,message->frame.data1);  //the steering mode
        Store_write(st(),EE_SPEED_FACTOR+2,message->frame.data2);  //the steering mode
        Store_write(st(),EE_SPEED_FACTOR+3,message->frame.data3);  //the steering mode
        break;

    case 2: //POWER FACTOR
        Store_write(st(),EE_POWER_FACTOR,message->frame.data0);  //the steering mode
        Store_write(st(),EE_POWER_FACTOR+1,message->frame.data1);  //the steering mode
        Store_write(st(),EE_POWER_FACTOR+2,message->frame.data2);  //the steering mode
        Store_write(st(),EE_POWER_FACTOR+3,message->frame.data3);  //the steering mode
        break;

    case 3: //STEERING FACTOR
        Store_write(st(),EE_STEERING_FACTOR,message->frame.data0);  //the steering mode
        Store_write(st(),EE_STEERING_FACTOR+1,message->frame.data1);  //the steering mode
        Store_write(st(),EE_STEERING_FACTOR+2,message->frame.data2);  //the steering mode
        Store_write(st(),EE_STEERING_FACTOR+3,message->frame.data3);  //the steering mode
        break;

    case 4: //SECURITY PARAMETERS
        Store_write(st(),EE_MAX_SPEED_FWD,message->frame.data0);  //the steering mode
        Store_write(st(),EE_MAX_SPEED_BWD,message->frame.data1); //the alive time  
        break;
    
    default:
        break;
    }
}

void storeJoystickData(CommControl* me,  uint8_t msgNbr,uCAN_MSG* message)
{
    static bool state = false;
    //Managing messages from the joystick
    switch (msgNbr)
    {
    case 1: //MEASURE
        Joystick_setPosX(js(), message->frame.data0);
        Joystick_setPosY(js(), message->frame.data1);
        Joystick_setButton(js(), message->frame.data2);
        break;

    case 2: //BRAKE
        if(state == false){
            if(message->frame.data0 == 0)
            {
                POST(drsm(), DriveSM_processEvent, evDRBrakeOn,0,0);
                state = true;
            }            
        }
        else{
            if(message->frame.data0 == 1)
            {
                POST(drsm(), DriveSM_processEvent, evDRBrakeOff,0,0);
                state = false;
            } 
        }
        break;

    case 0xF: //ALIVE
        break;
    
    default:
        break;
    }
}

void storeDisplayData(CommControl* me, uint8_t msgNbr, uCAN_MSG* message)
{
    //Managing messages from the display
    switch (msgNbr)
    {
    case 0xF: //ALIVE
        break;
    
    default:
        break;
    } 
}

void storeDriveData(CommControl* me, uint8_t msgNbr, uCAN_MSG* message)
{
    int32_t tmp = 0;
    //Managing messages from the drive
    switch (msgNbr)
    {
    case 0: //SPEED
        tmp += (uint32_t)(message->frame.data0)<<24;
        tmp += (uint32_t)(message->frame.data1)<<16;
        tmp += (uint32_t)(message->frame.data2)<<8;
        tmp += (message->frame.data3);
        dr()->speed_rpm = tmp;
        Drive_sendSpeed(dr());
        break;

    case 0xF: //ALIVE
        break;
    
    default:
        break;
    }
}

void storeSteeringData(CommControl* me, uint8_t msgNbr, uCAN_MSG* message)
{
    uint32_t tmp = 0;
    //Managing messages from the steering
    switch (msgNbr)
    {
    case 0: //HOMING & STATUS
        steer()->homing = message->frame.data0;
        steer()->status = message->frame.data1;
        break;

    case 1: //GET CENTER
        tmp = (uint32_t)message->frame.data0<<24;
        tmp += (uint32_t)message->frame.data1<<16;
        tmp += (uint32_t)message->frame.data2<<8;
        tmp += message->frame.data3;
        steer()->center_pos = tmp;
        break;   

    case 2: //GET POS
        tmp = (uint32_t)message->frame.data0<<24;
        tmp += (uint32_t)message->frame.data1<<16;
        tmp += (uint32_t)message->frame.data2<<8;
        tmp += message->frame.data3;
        steer()->actual_pos = tmp;
        break;     

    case 0xF: //ALIVE
        break;
    
    default:
        break;
    }
}

void storeSupplyData(CommControl* me, uint8_t msgNbr, uCAN_MSG* message)
{
    //Managing messages from the supply
    switch (msgNbr)
    {        
    case 0xF: //ALIVE
        break;
    
    default:
        break;
    }  
}
