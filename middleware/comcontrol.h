#ifndef CommControl_ONCE
#define CommControl_ONCE

#include <stdint.h>
#include "../mcc_generated_files/mcc.h"
#include "../xf/xf.h"

#define KEEPALIVEID   0x13F
#define MEASUREID     0x211
#define SETUPID     0x0
#define CONTROLID   0x1
#define JOYSTICKID  0x2
#define DISPLAYID   0x3
#define DRIVEID     0x4
#define STEERINGID  0x5
#define SUPPLYID    0x6
#define TIMEFACTOR    10

#define RECEIVETM 20


/*
 * this is a clever can id parser
 * see and learn
 */

struct CanId_ 
{
    unsigned msgNbr  :  4;
    unsigned dest    :  3;
    unsigned space   :  1;
    unsigned src     :  3;
    unsigned unused1 :  5;  
    unsigned unused2 :  8; //bitfields can not exceed 8 bit
    unsigned unused3 :  8; //the uCAN_MSG.id is an uint32_t
};
typedef struct CanId_ CanId;

union CanIdParser_
{
    CanId cid;
    uint32_t raw;
};
typedef union CanIdParser_ CanIdParser;

typedef enum CCEvent 
{ 
    evRXInit = 10,
    evRXDefault,     
    evRXReadTm,
    evTXInit,
    evTXDefault,     
    evTXSendAliveTm     
} CCEvent;

typedef enum CCRXSTate_
{
    ST_RXINIT=20, 
    ST_RXWAIT,
    ST_RXRCVMSG
} CCRXState;

typedef enum CCTXSTate_
{
    ST_TXINIT=30, 
    ST_TXWAIT,
    ST_TXSENDALIVEMSG
} CCTXState;

struct CommControl_
{
    uCAN_MSG txMsg;
    uCAN_MSG rxMsg;
    CCRXState rxState;
    CCTXState txState;
};

typedef struct CommControl_ CommControl;

void CommControl_init(CommControl* me);
void CommControl_startBehaviour(CommControl* me);
bool CommControl_processEvent(Event* ev);
void CommControl_sendCanMessage(CommControl* me, uCAN_MSG* message);

#endif
