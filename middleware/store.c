#include "store.h"
__EEPROM_DATA (0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00);
void Store_init(Store* me)
{
    uint8_t signature = DATAEE_ReadByte(EE_INIT);
    uint8_t signature2 = DATAEE_ReadByte(EE_INIT2);
    
    if((signature != 0xB4) || (signature2 != 0xAA))   // EEPROM never initialised
    {        
        DATAEE_WriteByte(EE_INIT,0xB4);
        DATAEE_WriteByte(EE_INIT2,0xAA);
        DATAEE_WriteByte(EE_STEERING_MODE,0);        // mode is on time
        DATAEE_WriteByte(EE_ALIVE_TM,10);      // alive each 100 ms
        DATAEE_WriteByte(EE_SPEED_FACTOR,0x00);  //the steering mode
        DATAEE_WriteByte(EE_SPEED_FACTOR+1,0x00);  //the steering mode
        DATAEE_WriteByte(EE_SPEED_FACTOR+2,0x67);  //the steering mode
        DATAEE_WriteByte(EE_SPEED_FACTOR+3,0xB6);  //the steering mode
        DATAEE_WriteByte(EE_POWER_FACTOR,0x00);  //the steering mode
        DATAEE_WriteByte(EE_POWER_FACTOR+1,0x00);  //the steering mode
        DATAEE_WriteByte(EE_POWER_FACTOR+2,0x27);  //the steering mode
        DATAEE_WriteByte(EE_POWER_FACTOR+3,0x10);  //the steering mode
        DATAEE_WriteByte(EE_STEERING_FACTOR,0x00);  //the steering mode
        DATAEE_WriteByte(EE_STEERING_FACTOR+1,0x00);  //the steering mode
        DATAEE_WriteByte(EE_STEERING_FACTOR+2,0x00);  //the steering mode
        DATAEE_WriteByte(EE_STEERING_FACTOR+3,0x00);  //the steering mode
        DATAEE_WriteByte(EE_MAX_SPEED_FWD,100);     // y send on change treshold
        DATAEE_WriteByte(EE_MAX_SPEED_BWD,100);     // y send on change treshold
        DATAEE_WriteByte(EE_CENTER_POS,0x00);     // y send on change treshold
        DATAEE_WriteByte(EE_CENTER_POS+1,0x00);     // y send on change treshold
        DATAEE_WriteByte(EE_CENTER_POS+2,0x00);     // y send on change treshold
        DATAEE_WriteByte(EE_CENTER_POS+3,0x00);     // y send on change treshold
        IO_RA1_SetLow();
    }
    
    me->eeValues[EE_INIT] = DATAEE_ReadByte(EE_INIT);
    me->eeValues[EE_STEERING_MODE] = DATAEE_ReadByte(EE_STEERING_MODE);
    me->eeValues[EE_ALIVE_TM] = DATAEE_ReadByte(EE_ALIVE_TM);
    me->eeValues[EE_SPEED_FACTOR] = DATAEE_ReadByte(EE_SPEED_FACTOR);
    me->eeValues[EE_SPEED_FACTOR+1] = DATAEE_ReadByte(EE_SPEED_FACTOR+1);
    me->eeValues[EE_SPEED_FACTOR+2] = DATAEE_ReadByte(EE_SPEED_FACTOR+2);
    me->eeValues[EE_SPEED_FACTOR+3] = DATAEE_ReadByte(EE_SPEED_FACTOR+3);
    me->eeValues[EE_POWER_FACTOR] = DATAEE_ReadByte(EE_POWER_FACTOR);
    me->eeValues[EE_POWER_FACTOR+1] = DATAEE_ReadByte(EE_POWER_FACTOR+1);
    me->eeValues[EE_POWER_FACTOR+2] = DATAEE_ReadByte(EE_POWER_FACTOR+2);
    me->eeValues[EE_POWER_FACTOR+3] = DATAEE_ReadByte(EE_POWER_FACTOR+3);
    me->eeValues[EE_STEERING_FACTOR] = DATAEE_ReadByte(EE_STEERING_FACTOR);
    me->eeValues[EE_STEERING_FACTOR+1] = DATAEE_ReadByte(EE_STEERING_FACTOR+1);
    me->eeValues[EE_STEERING_FACTOR+2] = DATAEE_ReadByte(EE_STEERING_FACTOR+2);
    me->eeValues[EE_STEERING_FACTOR+3] = DATAEE_ReadByte(EE_STEERING_FACTOR+3);
    me->eeValues[EE_MAX_SPEED_FWD] = DATAEE_ReadByte(EE_MAX_SPEED_FWD);
    me->eeValues[EE_MAX_SPEED_BWD] = DATAEE_ReadByte(EE_MAX_SPEED_BWD);
    me->eeValues[EE_CENTER_POS] = DATAEE_ReadByte(EE_CENTER_POS);
    me->eeValues[EE_CENTER_POS+1] = DATAEE_ReadByte(EE_CENTER_POS+1);
    me->eeValues[EE_CENTER_POS+2] = DATAEE_ReadByte(EE_CENTER_POS+2);
    me->eeValues[EE_CENTER_POS+3] = DATAEE_ReadByte(EE_CENTER_POS+3);
}

uint8_t Store_read(Store* me, uint16_t item)
{
    uint8_t value = 0;
    if (item < MAXEEVALUE)
    {
        value = me->eeValues[item];
    }
    return value;
}
void Store_write(Store* me, uint16_t item, uint8_t value)
{
    if (item < MAXEEVALUE)
    {
        me->eeValues[item] = value;
        DATAEE_WriteByte(item,value);
    }
}